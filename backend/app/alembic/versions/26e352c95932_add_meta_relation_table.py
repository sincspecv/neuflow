"""add meta relation table

Revision ID: 26e352c95932
Revises: 8b9892a839f4
Create Date: 2022-11-17 19:26:38.137150

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '26e352c95932'
down_revision = '8b9892a839f4'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "meta_relation_table",
        sa.Column('item_id', sa.Integer(), primary_key=True, nullable=False),
        sa.Column('item_meta_id', sa.Integer(), primary_key=True, nullable=False)
    )


def downgrade():
    op.drop_table("meta_relation_table")
