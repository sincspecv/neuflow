"""rename meta relation table

Revision ID: 68def607fa73
Revises: 26e352c95932
Create Date: 2022-11-17 19:36:21.953073

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '68def607fa73'
down_revision = '26e352c95932'
branch_labels = None
depends_on = None


def upgrade():
    op.drop_table("meta_relation_table")
    op.create_table(
        "item_meta_relation",
        sa.Column('item_id', sa.Integer(), primary_key=True, nullable=False),
        sa.Column('item_meta_id', sa.Integer(), primary_key=True, nullable=False)
    )


def downgrade():
    op.drop_table("item_meta_relation")
