"""add_relationship_column_to_item_table

Revision ID: 8b9892a839f4
Revises: 99ae2ee6d854
Create Date: 2022-11-15 18:28:55.986147

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '8b9892a839f4'
down_revision = '99ae2ee6d854'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column("item", sa.Column("meta", sa.PickleType()))
    op.add_column("item_meta", sa.Column("item", sa.Integer()))


def downgrade():
    pass
