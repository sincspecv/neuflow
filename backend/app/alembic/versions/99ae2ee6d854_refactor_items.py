"""refactor_items

Revision ID: 99ae2ee6d854
Revises: d4867f3a4c0a
Create Date: 2022-11-15 16:22:01.127605

"""
from alembic import op
from sqlalchemy.sql import func
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '99ae2ee6d854'
down_revision = 'd4867f3a4c0a'
branch_labels = None
depends_on = None


def upgrade():
    # ### Add created and updated columns to item table ### #
    op.add_column("item", sa.Column("created", sa.DateTime(timezone=True), server_default=func.now(), nullable=False))
    op.add_column("item", sa.Column("updated", sa.DateTime(timezone=True), server_default=func.now(), onupdate=func.now(), nullable=False))

    # ### Create item_meta table ### #
    op.create_table(
        "item_meta",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("meta_key", sa.String(), nullable=True),
        sa.Column("meta_value", sa.String(), nullable=True),
        sa.Column("item_id", sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(["item_id"], ["item.id"], ),
        sa.PrimaryKeyConstraint("id"),
    )
    op.create_index(op.f("ix_item_meta_meta_key"), "item_meta", ["meta_key"], unique=False)
    op.create_index(op.f("ix_item_meta_meta_value"), "item_meta", ["meta_value"], unique=False)
    op.create_index(op.f("ix_item_meta_id"), "item_meta", ["id"], unique=False)

    # ### Drop columns from item table that are now item meta ### #
    op.drop_column("item", "description")


def downgrade():
    pass
