"""add columns to item

Revision ID: 6b140d0abfcc
Revises: 68def607fa73
Create Date: 2022-11-20 18:47:53.398666

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '6b140d0abfcc'
down_revision = '68def607fa73'
branch_labels = None
depends_on = None


def upgrade():
    # Drop unneeded indexes from item_meta table
    op.drop_index(op.f("ix_item_meta_meta_key"), table_name="item_meta")
    op.drop_index(op.f("ix_item_meta_meta_value"), table_name="item_meta")

    # Add column to item table for meta that exists on every item
    op.add_column("item", sa.Column("type", sa.String()))
    op.add_column("item", sa.Column("description", sa.String()))
    op.add_column("item", sa.Column("slug", sa.String()))
    op.add_column("item", sa.Column("status", sa.String()))
    op.add_column("item", sa.Column("start", sa.DateTime()))

    # Add indexes for item table data
    op.create_index(op.f("ix_item_type"), "item", ["type"], unique=False)
    op.create_index(op.f("ix_item_slug"), "item", ["slug"], unique=False)
    op.create_index(op.f("ix_item_status"), "item", ["status"], unique=False)
    op.create_index(op.f("ix_item_start"), "item", ["start"], unique=False)


def downgrade():
    # Add indexes to item_meta table
    op.create_index(op.f("ix_item_meta_meta_key"), "item_meta", ["meta_key"], unique=False)
    op.create_index(op.f("ix_item_meta_meta_value"), "item_meta", ["meta_value"], unique=False)

    # Drop column from item table for meta that exists on every item
    op.drop_column("item", "type")
    op.drop_column("item", "description")
    op.drop_column("item", "slug")
    op.drop_column("item", "status")
    op.drop_column("item", "start")

    # Drop indexes from item table
    op.drop_index(op.f("ix_item_type"), table_name="item")
    op.drop_index(op.f("ix_item_slug"), table_name="item")
    op.drop_index(op.f("ix_item_status"), table_name="item")
    op.drop_index(op.f("ix_item_start"), table_name="item")
