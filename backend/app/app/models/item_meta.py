from typing import TYPE_CHECKING, List, Any

from sqlalchemy import Column, ForeignKey, Integer, String, JSON
from sqlalchemy.orm import relationship

from app.db.base_class import Base

if TYPE_CHECKING:
    from .item import Item  # noqa: F401

class ItemMeta(Base):
    __tablename__ = 'item_meta'

    id: int = Column(Integer, primary_key=True, index=True)
    meta_key = Column(String, index=False)
    meta_value = Column(String or JSON, index=False)
    item_id = Column(Integer, ForeignKey("item.id"))
    item: "Item" = relationship("Item", back_populates="meta")
