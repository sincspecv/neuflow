import datetime

from typing import TYPE_CHECKING, List

from sqlalchemy import Column, ForeignKey, Integer, String, DateTime, Enum
from sqlalchemy.sql import func
from sqlalchemy.orm import relationship

from fastapi.encoders import jsonable_encoder

from app import utils, enums
from app.db.base_class import Base

if TYPE_CHECKING:
    from .item_meta import ItemMeta
    from .user import User  # noqa: F401


class Item(Base):
    id: int = Column(Integer, primary_key=True, index=True)
    title = Column(String, index=True)
    created = Column(DateTime(timezone=True), server_default=func.now())
    updated = Column(DateTime(timezone=True), server_default=func.now(), onupdate=func.now())
    owner_id = Column(Integer, ForeignKey("user.id"))
    owner: "User" = relationship("User", back_populates="items")
    meta: List["ItemMeta"] = relationship("ItemMeta", back_populates="item")
    type = Column(Enum(enums.ItemType), index=True, nullable=False)
    description = Column(String, index=False)
    slug = Column(String, index=True, nullable=False)
    status = Column(Enum(enums.ItemStatus), index=True, nullable=False)
    start = Column(DateTime(timezone=False), index=True, nullable=False)

    def __init__(self, title: str, owner_id: int, meta: List, type: str, description: str, start: DateTime,  **kwargs):
        super().__init__(**kwargs)
        self.title = title
        self.owner_id = owner_id
        self.type = type
        self.description = description
        self.slug = utils.create_slug(self.title)
        self.start = start
