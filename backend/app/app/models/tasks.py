import datetime

from typing import TYPE_CHECKING, List

from sqlalchemy import Column, ForeignKey, Integer, String, DateTime, Enum
from sqlalchemy.sql import func
from sqlalchemy.orm import relationship

from app import utils, enums
from app.db.base_class import Base

if TYPE_CHECKING:
    from .item_meta import ItemMeta


class Task(Base):
    id: int = Column(Integer, primary_key=True, index=True)
    title: str = Column(String, index=True)
    description: str = Column(String, index=False)
    status: str = Column(Enum(enums.ItemStatus), index=True, nullable=False)
    created: datetime.datetime = Column(DateTime(timezone=True), server_default=func.now())
    updated: datetime.datetime = Column(DateTime(timezone=True), server_default=func.now(), onupdate=func.now())
    meta_id: int = Column(Integer, ForeignKey("item_meta.id"))

    def __init__(self, title: str, description: str, meta_id: int, status: str = "pending", **kwargs):
        super().__init__(**kwargs)
        self.title = title
        self.description = description
        self.status = status
        self.meta_id = meta_id
