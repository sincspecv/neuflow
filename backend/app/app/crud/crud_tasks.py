from collections import ChainMap, Counter
from typing import List

from fastapi.encoders import jsonable_encoder
from sqlalchemy import insert
from sqlalchemy.orm import Session
from sqlalchemy.sql import case, select

from app import crud, utils
from app.crud.base import CRUDBase
from app.models.tasks import Task
from app.schemas.tasks import TaskCreate, TaskUpdate


class CRUDTask(CRUDBase[Task, TaskCreate, TaskUpdate]):
    def create(
        self, db: Session, *, obj_in: TaskCreate, meta_id: int | None
    ) -> Task:
        task_obj = self.model(**obj_in, meta_id=meta_id)
        db.add(task_obj)
        db.commit()
        db.refresh(task_obj)

        return task_obj

    def get_by_meta(
        self, db: Session, *, meta_id: int, skip: int = 0, limit: int = 1000
    ) -> Task:
        return (
            db.query(self.model)
            .filter(Task.meta_id == meta_id)
            .offset(skip)
            .limit(limit)
            .all()
        )

    def prepare(
        self, db: Session, *, obj_in: TaskCreate, meta_id: int | None
    ) -> Task:
        return self.model(**obj_in, meta_id=meta_id)

    def update_multi(
        self,
        db: Session,
        *,
        db_obj: List[Task],
        obj_in: List[Task],
        meta_id: int,
    ) -> List[Task]:
        # TODO: Figure out a better way to handle this entire method

        update_obj = obj_in
        update_obj.extend(db_obj)
        update_data = []

        # Loop through all dicts in list to find updates
        for k, _item in enumerate(update_obj):
            item = jsonable_encoder(_item)
            if _item not in update_data:
                update_data.append(item)

                print("#### UPDATE DATA: ", update_data)

            # Loop through all items in dict to find and apply updates
            for i, d in enumerate(update_data):
                if d.get('meta_key', None) == item.get('meta_key', None):
                    if not d.get('meta_value', None) == item.get('meta_value', None):
                        # We found an update. Let's update the value in the dict
                        item['meta_value'] = d.get('meta_value', None)

                        # Now remove the update so we don't have a duplicate
                        del update_data[i]

        # Loop through update items and apply updates
        for i in range(len(update_data)):
            update_data[i] = self.model(**update_data[i])

            if update_data[i].id is not None:
                stmt = select(Task).where(Task.id == update_data[i].id)
                row = db.scalars(stmt).one()
                row.meta_value = update_data[i].meta_value
            else:
                db.add(update_data[i])

        db.commit()
        return db.query(Task).filter(Task.meta_id == meta_id).all()


tasks = CRUDTask(Task)
