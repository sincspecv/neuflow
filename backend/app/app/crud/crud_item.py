from __future__ import print_function
import inspect

from typing import Any, Dict, Generic, List, Optional, Type, TypeVar, Union, cast

from fastapi import Depends, HTTPException
from fastapi.encoders import jsonable_encoder
from pydantic import BaseModel
from sqlalchemy.orm import Session
from sqlalchemy.ext.associationproxy import association_proxy

from app import crud
from app.crud.base import CRUDBase
from app.models import Item, User, ItemMeta
from app.schemas.item import ItemCreate, ItemUpdate
from app.schemas.item_meta import ItemMetaCreate, ItemMetaUpdate
from app.db.base_class import Base

ModelType = TypeVar("ModelType", bound=Base)
CreateSchemaType = TypeVar("CreateSchemaType", bound=BaseModel)
UpdateSchemaType = TypeVar("UpdateSchemaType", bound=BaseModel)


class CRUDItem(CRUDBase[Item, ItemCreate, ItemUpdate]):
    def create_with_owner(
        self,
        db: Session,
        *, obj_in:
        ItemCreate,
        owner_id: int
    ) -> Item:
        item_data = jsonable_encoder(obj_in)
        item_obj = self.model(**item_data, owner_id=owner_id)
        db.add(item_obj)
        db.commit()
        db.refresh(item_obj)

        return item_obj

    def get_multi(
        self,
        db: Session,
        *,
        skip: 0,
        limit: 1000
    ) -> List[Item]:
        """
        SELECT item.id AS item_id, item.title AS item_title, item.created AS item_created, item.updated
        AS item_updated, item.owner_id AS item_owner_id FROM item LEFT OUTER JOIN item_meta ON item.id = item_meta.item_id
        LEFT OUTER JOIN (SELECT item.id = item_meta.item_id AS anon_2, item_meta.id AS id, item_meta.meta_key
        AS meta_key, item_meta.meta_value AS meta_value, item_meta.item_id AS item_id FROM item JOIN item_meta ON item_meta.item_id = item.id) AS anon_1 ON item.id = anon_1.item_id
        """

        sq = (
            db.query(Item.meta, ItemMeta)
            .join(ItemMeta, ItemMeta.item_id == Item.id)
            .subquery()
        )

        query = (
            db.query(Item)
            .outerjoin(Item.meta, sq)
        )

        return query.all()

    def get_item_by_id(
        self,
        db: Session,
        item_id: int
    ) -> Item:
        sq = (
            db.query(Item.meta, ItemMeta)
            .join(ItemMeta, ItemMeta.item_id == Item.id)
            .subquery()
        )

        query = (
            db.query(Item)
            .filter(Item.id == item_id)
            .outerjoin(Item.meta, sq)
        )

        return query.first()

    def get_multi_by_owner(
        self,
        db: Session,
        *,
        owner_id: int,
        skip: int = 0,
        limit: int = 100
    ) -> List[Item]:
        sq = (
            db.query(Item.meta, ItemMeta)
            .join(ItemMeta, ItemMeta.item_id == Item.id)
            .subquery()
        )

        query = (
            db.query(Item)
            .filter(Item.owner_id == owner_id)
            .outerjoin(Item.meta, sq)
        )

        return query.all()

    def update_item_by_id(
        self,
        db: Session,
        *,
        db_obj: Item,
        obj_in: Item
    ) -> Item:
        update_obj = obj_in.dict(exclude_unset=True)
        obj_data = jsonable_encoder(db_obj)
        if update_obj["meta"] is not None and len(update_obj["meta"]) > 0:
            # TODO: Figure out an efficient way to insert the meta with item
            meta = obj_data["meta"] if "meta" in obj_data else []
            meta_in = update_obj["meta"]
            meta.extend(meta_in)
            meta_data = []
            for m in meta:
                if m not in meta_data:
                    meta_data.append(jsonable_encoder(m))

            update_obj["meta"] = meta_data


        if isinstance(obj_in, dict):
            update_obj = obj_in

        for field in obj_data:
            if field in update_obj:
                setattr(db_obj, field, update_obj[field])

        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj


item = CRUDItem(Item)
