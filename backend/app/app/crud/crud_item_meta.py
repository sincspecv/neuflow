import json

from collections import ChainMap, Counter
from typing import List, Dict

from fastapi.encoders import jsonable_encoder
from sqlalchemy import insert
from sqlalchemy.orm import Session
from sqlalchemy.sql import case, select

from app import crud, utils
from app.crud.base import CRUDBase
from app.models.item_meta import ItemMeta
from app.schemas.item_meta import ItemMetaCreate, ItemMetaUpdate


class CRUDItemMeta(CRUDBase[ItemMeta, ItemMetaCreate, ItemMetaUpdate]):
    def create(
        self, db: Session, *, obj_in: ItemMetaCreate, item_id: int | None
    ) -> ItemMeta:
        data_obj = jsonable_encoder(obj_in)
        # We can't add a list or dict to the db, so we need to cast it to a string
        if isinstance(data_obj["meta_value"], list or dict):
            obj_in["meta_value"] = json.dumps(data_obj["meta_value"])

        item_meta_obj = self.model(**obj_in, item_id=item_id)

        db.add(item_meta_obj)
        db.commit()
        db.refresh(item_meta_obj)

        item_meta_obj = jsonable_encoder(item_meta_obj)

        if item_meta_obj["meta_value"][:1] == "[" or item_meta_obj["meta_value"][:1] == "{":
            item_meta_obj["meta_value"] = json.loads(item_meta_obj["meta_value"])

        return item_meta_obj

    def get_by_item(
        self, db: Session, *, item_id: int, skip: int = 0, limit: int = 1000
    ) -> ItemMeta:
        return (
            db.query(self.model)
            .filter(ItemMeta.item_id == item_id)
            .offset(skip)
            .limit(limit)
            .all()
        )

    def prepare(
        self, db: Session, *, obj_in: ItemMetaCreate, item_id: int | None
    ) -> ItemMeta:
        return self.model(**obj_in, item_id=item_id)

    def update_multi(
        self,
        db: Session,
        *,
        db_obj: List[ItemMeta],
        obj_in: List[ItemMeta],
        item_id: int,
    ) -> List[ItemMeta]:
        # TODO: Figure out a better way to handle this entire method

        update_obj = obj_in
        update_obj.extend(db_obj)
        update_data = []

        # Loop through all dicts in list to find updates
        for k, _item in enumerate(update_obj):
            item = jsonable_encoder(_item)
            if _item not in update_data:
                update_data.append(item)

                print("#### UPDATE DATA: ", update_data)

            # Loop through all items in dict to find and apply updates
            for i, d in enumerate(update_data):
                if d.get('meta_key', None) == item.get('meta_key', None):
                    if not d.get('meta_value', None) == item.get('meta_value', None):
                        # We found an update. Let's update the value in the dict
                        item['meta_value'] = d.get('meta_value', None)

                        # Now remove the update so we don't have a duplicate
                        del update_data[i]

        # Loop through update items and apply updates
        for i in range(len(update_data)):
            update_data[i] = self.model(**update_data[i])

            if update_data[i].id is not None:
                stmt = select(ItemMeta).where(ItemMeta.id == update_data[i].id)
                row = db.scalars(stmt).one()
                row.meta_value = update_data[i].meta_value
            else:
                db.add(update_data[i])

        db.commit()
        return db.query(ItemMeta).filter(ItemMeta.item_id == item_id).all()


item_meta = CRUDItemMeta(ItemMeta)
