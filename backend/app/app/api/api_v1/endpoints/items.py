import json

from typing import Any, List, Optional

from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from app import crud, models, schemas, enums, utils
from app.api import deps

router = APIRouter()


@router.post("/", response_model=schemas.Item, status_code=status.HTTP_201_CREATED)
def create_item(
    *,
    db: Session = Depends(deps.get_db),
    item_in: schemas.ItemCreate,
    current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Create new item.
    """
    # Grab a copy of our item meta before deleting it.
    item_meta = item_in.meta

    item_in.meta = []

    # Add the item to the item table
    item = crud.item.create_with_owner(db=db, obj_in=item_in, owner_id=current_user.id)
    item_data = jsonable_encoder(item)  # JSON object of the item we just added to the database

    # Add our meta data to the database
    meta_data = []
    for m in item_meta:
        del m.item_id
        m_data = jsonable_encoder(m)

        # We can't add a list or dict to the db, so we need to cast it to a string
        # if isinstance(m_data["meta_value"], list or dict):
        #     m_data["meta_value"] = json.dumps(m_data["meta_value"])

        # Add that bitch
        meta = crud.item_meta.create(db=db, obj_in=m_data, item_id=item_data['id'])
        # meta_obj = jsonable_encoder(meta)  # JSON object of the meta we just added to db

        # If we stored a list or dict in the database, we need to convert it back before returning
        # if meta_obj["meta_value"][:1] == "[" or meta_obj["meta_value"][:1] == "{":
        #     meta_obj["meta_value"] = json.loads(meta_obj["meta_value"])

        meta_data.append(meta)  

    item_data['meta'] = meta_data

    return item_data

@router.get("/", response_model=List[schemas.Item])
def get_all_items(
    db: Session = Depends(deps.get_db),
    skip: int = 0,
    limit: int = 100,
    current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Retrieve all items. (Superuser only)
    """
    if crud.user.is_superuser(current_user):
        items = crud.item.get_multi(db, skip=skip, limit=limit)
    else:
        items = crud.item.get_multi_by_owner(
            db=db, owner_id=current_user.id, skip=skip, limit=limit
        )
    return items


@router.put("/{item_id}", response_model=schemas.Item)
def update_item(
    *,
    db: Session = Depends(deps.get_db),
    item_id: int,
    item_in: schemas.ItemUpdate,
    current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Update an item.
    """
    item = crud.item.get_item_by_id(db=db, item_id=item_id)
    if not item:
        raise HTTPException(status_code=404, detail="Item not found")
    if not crud.user.is_superuser(current_user) and (item.owner_id != current_user.id):
        raise HTTPException(status_code=400, detail="Not enough permissions")
    if item_in.meta is not None:
        for m in item_in.meta:
            m.item_id = item_id

        crud.item_meta.update_multi(db=db, obj_in=item_in.meta, db_obj=item.meta, item_id=item_id)

    item = crud.item.update_item_by_id(db=db, db_obj=item, obj_in=item_in)
    return item


@router.get("/{item_id}", response_model=schemas.Item)
def get_item(
    *,
    db: Session = Depends(deps.get_db),
    item_id: int,
    current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Get item by item ID.
    """
    item = crud.item.get_item_by_id(db=db, item_id=item_id)
    if not item:
        raise HTTPException(status_code=404, detail="Item not found")
    if not crud.user.is_superuser(current_user) and (item.owner_id != current_user.id):
        raise HTTPException(status_code=400, detail="Not enough permissions")
    return item


@router.delete("/{id}", response_model=schemas.Item)
def delete_item(
    *,
    db: Session = Depends(deps.get_db),
    id: int,
    current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Delete an item.
    """
    item = crud.item.get(db=db, id=id)
    if not item:
        raise HTTPException(status_code=404, detail="Item not found")
    if not crud.user.is_superuser(current_user) and (item.owner_id != current_user.id):
        raise HTTPException(status_code=400, detail="Not enough permissions")
    item = crud.item.remove(db=db, id=id)
    return item

@router.get("/owner/{id}", response_model=List[schemas.Item])
def get_items_by_owner(
    *,
    db: Session = Depends(deps.get_db),
    id: int,
    current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Get items by user (owner) ID.
    """
    item = crud.item.get_multi_by_owner(db=db, owner_id=id)
    if not item:
        raise HTTPException(status_code=404, detail="Item not found")
    if not crud.user.is_superuser(current_user) and (item.owner_id != current_user.id):
        raise HTTPException(status_code=400, detail="Permissions error: Request not allowed.")
    return item
