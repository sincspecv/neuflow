from typing import TYPE_CHECKING, List

from sqlalchemy import Column, ForeignKey, Integer, String, DateTime, Table

from .base_class import Base

meta_relation_table = Table('item_meta_relation', Base.metadata,
                    Column('item_id', Integer, ForeignKey('item.id'), primary_key=True),
                    Column('item_meta_id', Integer, ForeignKey('item_meta.id'), primary_key=True))