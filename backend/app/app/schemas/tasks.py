import datetime

from typing import Optional, List

from pydantic import BaseModel

from .item_meta import ItemMeta
from app import enums


# Shared properties
class TaskBase(BaseModel):
    title: Optional[str] = None
    description: Optional[str] = None
    status: Optional[enums.TaskStatus] = None

    class Config:
        orm_mode = True
        allow_population_by_field_name = True


# Properties to receive on Task creation
class TaskCreate(TaskBase):
    pass

# Properties to receive on Task update
class TaskUpdate(TaskBase):
    pass

# Properties shared by models stored in DB
class TaskInDBBase(TaskBase):
    id: int
    meta_id: int
    created: datetime.datetime
    updated: datetime.datetime


# Properties to return to client
class Task(TaskInDBBase):
    pass

    class Config:
        orm_mode = True
        allow_population_by_field_name = True


# Properties properties stored in DB
class TaskInDB(TaskInDBBase):
    pass
