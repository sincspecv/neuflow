from typing import Optional, Any

from sqlalchemy import JSON

from pydantic import BaseModel


# Shared properties
class ItemMetaBase(BaseModel):
    meta_key: str | int
    meta_value: Any


# Properties to receive on item creation
class ItemMetaCreate(ItemMetaBase):
    item_id: Optional[int] = None


# Properties to receive on item update
class ItemMetaUpdate(ItemMetaBase):
    id: Optional[int] = None


# Properties shared by models stored in DB
class ItemMetaInDBBase(ItemMetaBase):
    id: int
    item_id: int

    class Config:
        orm_mode = True
        allow_population_by_field_name = True


# Properties to return to client
class ItemMeta(ItemMetaInDBBase):
    id: Optional[int] = None
    item_id: Optional[int] = None

# Properties properties stored in DB
class ItemMetaInDB(ItemMetaInDBBase):
    pass
