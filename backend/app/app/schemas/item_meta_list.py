from typing import Optional, List

from pydantic import BaseModel

from .item_meta import ItemMeta


# Shared properties
class ItemMetaListBase(BaseModel):
    List: Optional[ItemMeta]

    class Config:
        orm_mode = True


# Properties to receive on item creation
class ItemMetaListCreate(ItemMetaListBase):
    List: Optional[ItemMeta]

    class Config:
        orm_mode = True


# Properties to receive on item update
class ItemMetaListUpdate(ItemMetaListBase):
    List: Optional[ItemMeta]

    class Config:
        orm_mode = True


# Properties shared by models stored in DB
class ItemMetaListInDBBase(ItemMetaListBase):
    List: Optional[ItemMeta]

    class Config:
        orm_mode = True


# Properties to return to client
class ItemMetaList(ItemMetaListInDBBase):
    [Optional[ItemMeta]]

    class Config:
        orm_mode = True

# Properties properties stored in DB
class ItemMetaListInDB(ItemMetaListInDBBase):
    pass
