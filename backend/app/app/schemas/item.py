import datetime

from typing import Optional, List

from pydantic import BaseModel

from .item_meta import ItemMeta
from app import enums


# Shared properties
class ItemBase(BaseModel):
    title: Optional[str] = None
    meta: Optional[List[ItemMeta]] = None
    type: Optional[enums.ItemType] = None
    description: Optional[str] = None
    slug: Optional[str] = None
    status: Optional[enums.ItemStatus] = None
    start: Optional[datetime.datetime] = None

class Config:
    orm_mode = True
    allow_population_by_field_name = True


# Properties to receive on item creation
class ItemCreate(ItemBase):
    pass

# Properties to receive on item update
class ItemUpdate(ItemBase):
    type: Optional[enums.ItemType] = None
    slug: Optional[str]
    status: Optional[enums.ItemStatus] = None

# Properties shared by models stored in DB
class ItemInDBBase(ItemBase):
    id: int
    title: str
    meta: Optional[List[ItemMeta]] = None
    created: datetime.datetime
    updated: datetime.datetime
    owner_id: int


# Properties to return to client
class Item(ItemInDBBase):
    id: int
    title: str
    meta: Optional[List[ItemMeta]] = None
    created: datetime.datetime
    updated: datetime.datetime

    class Config:
        orm_mode = True
        allow_population_by_field_name = True


# Properties properties stored in DB
class ItemInDB(ItemInDBBase):
    pass
