import enum
from sqlalchemy import Enum


class ItemType(enum.Enum):
    basic = "basic"
    recurring = "recurring"
    project = "project"
    bill = "bill"
    work = "work"
    appointment = "appointment"
    event = "event"


class ItemStatus(enum.Enum):
    active = "active"
    inactive = "inactive"
    pending = "pending"
    complete = "complete"
    canceled = "canceled"


class TaskStatus(enum.Enum):
    active = "active"
    inactive = "inactive"
    pending = "pending"
    complete = "complete"
    canceled = "canceled"
