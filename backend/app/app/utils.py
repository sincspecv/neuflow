import logging
from datetime import datetime, timedelta
from pathlib import Path
from typing import Any, Dict, Optional

import emails
from emails.template import JinjaTemplate
from jose import jwt

from fastapi.encoders import jsonable_encoder

from app.core.config import settings


def send_email(
    email_to: str,
    subject_template: str = "",
    html_template: str = "",
    environment: Dict[str, Any] = {},
) -> None:
    assert settings.EMAILS_ENABLED, "no provided configuration for email variables"
    message = emails.Message(
        subject=JinjaTemplate(subject_template),
        html=JinjaTemplate(html_template),
        mail_from=(settings.EMAILS_FROM_NAME, settings.EMAILS_FROM_EMAIL),
    )
    smtp_options = {"host": settings.SMTP_HOST, "port": settings.SMTP_PORT}
    if settings.SMTP_TLS:
        smtp_options["tls"] = True
    if settings.SMTP_USER:
        smtp_options["user"] = settings.SMTP_USER
    if settings.SMTP_PASSWORD:
        smtp_options["password"] = settings.SMTP_PASSWORD
    response = message.send(to=email_to, render=environment, smtp=smtp_options)
    logging.info(f"send email result: {response}")


def send_test_email(email_to: str) -> None:
    project_name = settings.PROJECT_NAME
    subject = f"{project_name} - Test email"
    with open(Path(settings.EMAIL_TEMPLATES_DIR) / "test_email.html") as f:
        template_str = f.read()
    send_email(
        email_to=email_to,
        subject_template=subject,
        html_template=template_str,
        environment={"project_name": settings.PROJECT_NAME, "email": email_to},
    )


def send_reset_password_email(email_to: str, email: str, token: str) -> None:
    project_name = settings.PROJECT_NAME
    subject = f"{project_name} - Password recovery for user {email}"
    with open(Path(settings.EMAIL_TEMPLATES_DIR) / "reset_password.html") as f:
        template_str = f.read()
    server_host = settings.SERVER_HOST
    link = f"{server_host}/reset-password?token={token}"
    send_email(
        email_to=email_to,
        subject_template=subject,
        html_template=template_str,
        environment={
            "project_name": settings.PROJECT_NAME,
            "username": email,
            "email": email_to,
            "valid_hours": settings.EMAIL_RESET_TOKEN_EXPIRE_HOURS,
            "link": link,
        },
    )


def send_new_account_email(email_to: str, username: str, password: str) -> None:
    project_name = settings.PROJECT_NAME
    subject = f"{project_name} - New account for user {username}"
    with open(Path(settings.EMAIL_TEMPLATES_DIR) / "new_account.html") as f:
        template_str = f.read()
    link = settings.SERVER_HOST
    send_email(
        email_to=email_to,
        subject_template=subject,
        html_template=template_str,
        environment={
            "project_name": settings.PROJECT_NAME,
            "username": username,
            "password": password,
            "email": email_to,
            "link": link,
        },
    )


def generate_password_reset_token(email: str) -> str:
    delta = timedelta(hours=settings.EMAIL_RESET_TOKEN_EXPIRE_HOURS)
    now = datetime.utcnow()
    expires = now + delta
    exp = expires.timestamp()
    encoded_jwt = jwt.encode(
        {"exp": exp, "nbf": now, "sub": email},
        settings.SECRET_KEY,
        algorithm="HS256",
    )
    return encoded_jwt


def verify_password_reset_token(token: str) -> Optional[str]:
    try:
        decoded_token = jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])
        return decoded_token["email"]
    except jwt.JWTError:
        return None

def create_slug(slug: str) -> Optional[str]:
    # TODO: Handle duplicate slugs already in the database
    translate_chars = {
        'ª': 'a',
        'º': 'o',
        'À': 'A',
        'Á': 'A',
        'Â': 'A',
        'Ã': 'A',
        'Ä': 'A',
        'Å': 'A',
        'Æ': 'AE',
        'Ç': 'C',
        'È': 'E',
        'É': 'E',
        'Ê': 'E',
        'Ë': 'E',
        'Ì': 'I',
        'Í': 'I',
        'Î': 'I',
        'Ï': 'I',
        'Ð': 'D',
        'Ñ': 'N',
        'Ò': 'O',
        'Ó': 'O',
        'Ô': 'O',
        'Õ': 'O',
        'Ö': 'O',
        'Ù': 'U',
        'Ú': 'U',
        'Û': 'U',
        'Ü': 'U',
        'Ý': 'Y',
        'Þ': 'TH',
        'ß': 's',
        'à': 'a',
        'á': 'a',
        'â': 'a',
        'ã': 'a',
        'ä': 'a',
        'å': 'a',
        'æ': 'ae',
        'ç': 'c',
        'è': 'e',
        'é': 'e',
        'ê': 'e',
        'ë': 'e',
        'ì': 'i',
        'í': 'i',
        'î': 'i',
        'ï': 'i',
        'ð': 'd',
        'ñ': 'n',
        'ò': 'o',
        'ó': 'o',
        'ô': 'o',
        'õ': 'o',
        'ö': 'o',
        'ø': 'o',
        'ù': 'u',
        'ú': 'u',
        'û': 'u',
        'ü': 'u',
        'ý': 'y',
        'þ': 'th',
        'ÿ': 'y',
        'Ø': 'O',
        'Ā': 'A',
        'ā': 'a',
        'Ă': 'A',
        'ă': 'a',
        'Ą': 'A',
        'ą': 'a',
        'Ć': 'C',
        'ć': 'c',
        'Ĉ': 'C',
        'ĉ': 'c',
        'Ċ': 'C',
        'ċ': 'c',
        'Č': 'C',
        'č': 'c',
        'Ď': 'D',
        'ď': 'd',
        'Đ': 'D',
        'đ': 'd',
        'Ē': 'E',
        'ē': 'e',
        'Ĕ': 'E',
        'ĕ': 'e',
        'Ė': 'E',
        'ė': 'e',
        'Ę': 'E',
        'ę': 'e',
        'Ě': 'E',
        'ě': 'e',
        'Ĝ': 'G',
        'ĝ': 'g',
        'Ğ': 'G',
        'ğ': 'g',
        'Ġ': 'G',
        'ġ': 'g',
        'Ģ': 'G',
        'ģ': 'g',
        'Ĥ': 'H',
        'ĥ': 'h',
        'Ħ': 'H',
        'ħ': 'h',
        'Ĩ': 'I',
        'ĩ': 'i',
        'Ī': 'I',
        'ī': 'i',
        'Ĭ': 'I',
        'ĭ': 'i',
        'Į': 'I',
        'į': 'i',
        'İ': 'I',
        'ı': 'i',
        'Ĳ': 'IJ',
        'ĳ': 'ij',
        'Ĵ': 'J',
        'ĵ': 'j',
        'Ķ': 'K',
        'ķ': 'k',
        'ĸ': 'k',
        'Ĺ': 'L',
        'ĺ': 'l',
        'Ļ': 'L',
        'ļ': 'l',
        'Ľ': 'L',
        'ľ': 'l',
        'Ŀ': 'L',
        'ŀ': 'l',
        'Ł': 'L',
        'ł': 'l',
        'Ń': 'N',
        'ń': 'n',
        'Ņ': 'N',
        'ņ': 'n',
        'Ň': 'N',
        'ň': 'n',
        'ŉ': 'n',
        'Ŋ': 'N',
        'ŋ': 'n',
        'Ō': 'O',
        'ō': 'o',
        'Ŏ': 'O',
        'ŏ': 'o',
        'Ő': 'O',
        'ő': 'o',
        'Œ': 'OE',
        'œ': 'oe',
        'Ŕ': 'R',
        'ŕ': 'r',
        'Ŗ': 'R',
        'ŗ': 'r',
        'Ř': 'R',
        'ř': 'r',
        'Ś': 'S',
        'ś': 's',
        'Ŝ': 'S',
        'ŝ': 's',
        'Ş': 'S',
        'ş': 's',
        'Š': 'S',
        'š': 's',
        'Ţ': 'T',
        'ţ': 't',
        'Ť': 'T',
        'ť': 't',
        'Ŧ': 'T',
        'ŧ': 't',
        'Ũ': 'U',
        'ũ': 'u',
        'Ū': 'U',
        'ū': 'u',
        'Ŭ': 'U',
        'ŭ': 'u',
        'Ů': 'U',
        'ů': 'u',
        'Ű': 'U',
        'ű': 'u',
        'Ų': 'U',
        'ų': 'u',
        'Ŵ': 'W',
        'ŵ': 'w',
        'Ŷ': 'Y',
        'ŷ': 'y',
        'Ÿ': 'Y',
        'Ź': 'Z',
        'ź': 'z',
        'Ż': 'Z',
        'ż': 'z',
        'Ž': 'Z',
        'ž': 'z',
        'ſ': 's',
        'Ș': 'S',
        'ș': 's',
        'Ț': 'T',
        'ț': 't',
        '€': 'E',
        '£': '',
        'Ơ': 'O',
        'ơ': 'o',
        'Ư': 'U',
        'ư': 'u',
        'Ầ': 'A',
        'ầ': 'a',
        'Ằ': 'A',
        'ằ': 'a',
        'Ề': 'E',
        'ề': 'e',
        'Ồ': 'O',
        'ồ': 'o',
        'Ờ': 'O',
        'ờ': 'o',
        'Ừ': 'U',
        'ừ': 'u',
        'Ỳ': 'Y',
        'ỳ': 'y',
        'Ả': 'A',
        'ả': 'a',
        'Ẩ': 'A',
        'ẩ': 'a',
        'Ẳ': 'A',
        'ẳ': 'a',
        'Ẻ': 'E',
        'ẻ': 'e',
        'Ể': 'E',
        'ể': 'e',
        'Ỉ': 'I',
        'ỉ': 'i',
        'Ỏ': 'O',
        'ỏ': 'o',
        'Ổ': 'O',
        'ổ': 'o',
        'Ở': 'O',
        'ở': 'o',
        'Ủ': 'U',
        'ủ': 'u',
        'Ử': 'U',
        'ử': 'u',
        'Ỷ': 'Y',
        'ỷ': 'y',
        'Ẫ': 'A',
        'ẫ': 'a',
        'Ẵ': 'A',
        'ẵ': 'a',
        'Ẽ': 'E',
        'ẽ': 'e',
        'Ễ': 'E',
        'ễ': 'e',
        'Ỗ': 'O',
        'ỗ': 'o',
        'Ỡ': 'O',
        'ỡ': 'o',
        'Ữ': 'U',
        'ữ': 'u',
        'Ỹ': 'Y',
        'ỹ': 'y',
        'Ấ': 'A',
        'ấ': 'a',
        'Ắ': 'A',
        'ắ': 'a',
        'Ế': 'E',
        'ế': 'e',
        'Ố': 'O',
        'ố': 'o',
        'Ớ': 'O',
        'ớ': 'o',
        'Ứ': 'U',
        'ứ': 'u',
        'Ạ': 'A',
        'ạ': 'a',
        'Ậ': 'A',
        'ậ': 'a',
        'Ặ': 'A',
        'ặ': 'a',
        'Ẹ': 'E',
        'ẹ': 'e',
        'Ệ': 'E',
        'ệ': 'e',
        'Ị': 'I',
        'ị': 'i',
        'Ọ': 'O',
        'ọ': 'o',
        'Ộ': 'O',
        'ộ': 'o',
        'Ợ': 'O',
        'ợ': 'o',
        'Ụ': 'U',
        'ụ': 'u',
        'Ự': 'U',
        'ự': 'u',
        'Ỵ': 'Y',
        'ỵ': 'y',
        'ɑ': 'a',
        'Ǖ': 'U',
        'ǖ': 'u',
        'Ǘ': 'U',
        'ǘ': 'u',
        'Ǎ': 'A',
        'ǎ': 'a',
        'Ǐ': 'I',
        'ǐ': 'i',
        'Ǒ': 'O',
        'ǒ': 'o',
        'Ǔ': 'U',
        'ǔ': 'u',
        'Ǚ': 'U',
        'ǚ': 'u',
        'Ǜ': 'U',
        'ǜ': 'u',
    }

    slug = slug.translate(translate_chars)
    slug = slug.replace(' ', '-')
    slug = slug.lower()

    return slug


def get_dict_from_list(lst: list, key: str, value: str | None) -> Optional[dict]:
    return next(item for item in jsonable_encoder(lst) if item[key] == value)
