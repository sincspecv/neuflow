// Import Component hooks before component definitions
import "./component-hooks";
import Vue from "vue";
import { plugin, defaultConfig } from "@formkit/vue";
import App from "./App.vue";
import router from "./router";
import store from "@/store";
import "./registerServiceWorker";

Vue.config.productionTip = false;
Vue.use(plugin, defaultConfig);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
