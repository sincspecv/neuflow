/**
 * Color scheme generation from https://palx.jxnblk.com/
 * Source color: #215a87
 * @type {{current: string, gray: {"100": string, "200": string, "300": string, "400": string, "500": string, "600": string, "700": string, "800": string, "900": string, "50": string}, white: string, blue: {"100": string, "200": string, "300": string, "400": string, "500": string, "600": string, "700": string, "800": string, "900": string, "50": string}, inherit: string, black: string, indigo: {"100": string, "200": string, "300": string, "400": string, "500": string, "600": string, "700": string, "800": string, "900": string, "50": string}, transparent: string}}
 */

const customColors = {
  inherit: 'inherit',
  current: 'currentColor',
  transparent: 'transparent',
  black: '#050D15',
  white: '#fbfbfb',
  gray: {
    '50': '#f9f9fa',
    '100': '#eceeef',
    '200': '#dfe1e3',
    '300': '#d0d3d6',
    '400': '#c0c4c8',
    '500': '#afb4b8',
    '600': '#9aa1a6',
    '700': '#828a91',
    '800': '#646d76',
    '900': '#384048'
  },
  blue: {
    '50': '#f8fafb',
    '100': '#e8eef3',
    '200': '#d8e2ea',
    '300': '#c6d5e0',
    '400': '#b3c7d6',
    '500': '#9db6ca',
    '600': '#84a4bc',
    '700': '#658eac',
    '800': '#3e7197',
    '900': '#184262',
  },
  indigo: {
    '50': '#f9f9fc',
    '100': '#ecedf5',
    '200': '#dfe0ee',
    '300': '#d0d2e6',
    '400': '#c0c2dd',
    '500': '#aeb1d3',
    '600': '#999dc8',
    '700': '#8185bb',
    '800': '#6166aa',
    '900': '#2e348e'
  },
  violet: {
    '50': '#faf9fc',
    '100': '#f0ecf5',
    '200': '#e5deed',
    '300': '#d9cfe5',
    '400': '#cbbfdc',
    '500': '#bdacd2',
    '600': '#ac97c7',
    '700': '#977eb9',
    '800': '#7d5da7',
    '900': '#51268a'
  },
  red: {
    '50': '#fcf9f9',
    '100': '#f4ebec',
    '200': '#edddde',
    '300': '#e4cecf',
    '400': '#dbbdbf',
    '500': '#d1a9ac',
    '600': '#c59397',
    '700': '#b6787d',
    '800': '#a3555a',
    '900': '#781d24'
  },
  cyan: {
    '50': '#f6fafa',
    '100': '#e3f0ef',
    '200': '#cfe5e4',
    '300': '#b9d9d7',
    '400': '#a1ccc9',
    '500': '#85bdb9',
    '600': '#65aca7',
    '700': '#3e9690',
    '800': '#1d7872',
    '900': '#114743'
  }
}

module.exports = customColors;
