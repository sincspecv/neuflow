import { createApp } from 'vue'
import { createPinia } from 'pinia'
import { plugin, defaultConfig } from '@formkit/vue'
import { createProPlugin, inputs } from '@formkit/pro'
const pro = createProPlugin('fk-6abd90619e', inputs)
// import tailwindcss from 'tailwindcss';



import App from './App.vue'
import router from './router'

import './assets/main.css'

const app = createApp(App)

// FormKit
app.use(plugin, defaultConfig({ plugins: [pro] }))

app.use(router)
app.use(createPinia())

app.mount('#app')
