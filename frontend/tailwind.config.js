/** @type {import("@types/tailwindcss/tailwind-config").TailwindConfig } */

const customColors = require('./tailwind.config.colors')
const formKitTailwind = require('@formkit/themes/tailwindcss')

module.exports = {
  mode: 'jit',
  content: ['./index.html', './src/**/*.{vue,js,ts,jsx,css}', './formkit.config.ts'],
  theme: {
    colors: customColors,
    extend: {
      fontFamily: {
        'sans': ['ui-sans-serif', 'system-ui'],
        'serif': ['ui-serif', 'Georgia'],
        'mono': ['ui-monospace', 'SFMono-Regular'],
        'display': ['"Karla"', 'ui-sans-serif'],
        'body': ['"Quicksand"', 'ui-sans-serif'],
      }
    }
  },
  daisyui: {
    themes: [
      {
        nf_theme: {
          "--white": "#fbfbfb",
          "--black": '#050D15',
          "primary": "#005f8f",
          "secondary": "#4a86a8",
          "accent": "#7baec1",
          "neutral": "#afd7dc",
          "base-100": "#fbfbfb", // #384048
          "info": "#3ABFF8",
          "success": "#36D399",
          "warning": "#FBBD23",
          "error": "#93003a",
          ...customColors
        },
      },
    ],
  },
  plugins: [
    require('@tailwindcss/forms'),
    require('@tailwindcss/typography'),
    require('@tailwindcss/line-clamp'),
    require('@tailwindcss/aspect-ratio'),
    formKitTailwind,
    require("daisyui"),
  ],
}
