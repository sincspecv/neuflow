import { close, down, fileDoc, check, circle, spinner, star, trash, add, arrowUp, arrowDown } from "@formkit/icons"
import { DefaultConfigOptions } from '@formkit/vue'
import { createProPlugin, inputs } from '@formkit/pro'

const pro = createProPlugin('fk-6abd90619e', inputs)

const config: DefaultConfigOptions = {
  plugins: [pro],
  icons: { close, down, fileDoc, check, circle, spinner, star, trash, add, arrowUp, arrowDown, checkboxDecorator: check }
}

export default config
